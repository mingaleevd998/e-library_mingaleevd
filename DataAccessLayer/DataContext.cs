﻿using DataAccessLayer.Configurations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;


namespace DataAccessLayer
{
    public sealed class DataContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Issuance> Issuance  { get; set; }
        public DbSet<Copy> Copies { get; set; }
        public DbSet<User> Users { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureCreated();
            //Database.Migrate();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            FillDataExtension.FillData(modelBuilder);
            
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(BookConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IssuanceConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CopyConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly);
        }
    }
}

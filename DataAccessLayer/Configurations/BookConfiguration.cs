﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasKey(book => book.Id);
            builder.Property(book => book.Name).IsRequired();
            builder.Property(book => book.Author).IsRequired();
            builder.Property(book => book.YearOfPublishing).IsRequired();
            builder.Property(book => book.NumberOfCopies).IsRequired();
        }
    }
}
﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class IssuanceConfiguration: IEntityTypeConfiguration<Issuance>
    {
        public void Configure(EntityTypeBuilder<Issuance> builder)
        {
            builder.HasKey(issuance => issuance.Id);
            builder.Property(issuance => issuance.DateOfIssue).IsRequired();
        }
    }
}
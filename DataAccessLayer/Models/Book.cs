﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int YearOfPublishing { get; set; }
        public int NumberOfPages { get; set; }
        public int NumberOfCopies { get; set; }
    }
}
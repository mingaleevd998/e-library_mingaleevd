﻿namespace DataAccessLayer.Models
{
    public enum Role
    {
        Disable = -1,
        Student = 0,
        Teacher = 1,
        Librarian = 2,
        Admin = 3,
    }
}
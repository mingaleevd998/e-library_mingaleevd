﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Copy
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public bool AreAvailable { get; set; }
    }
}
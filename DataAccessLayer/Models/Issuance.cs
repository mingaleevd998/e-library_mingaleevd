﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Issuance
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int CopyId { get; set; }
        public Copy Copy { get; set; }
        public string DateOfIssue { get; set; }
    }
}
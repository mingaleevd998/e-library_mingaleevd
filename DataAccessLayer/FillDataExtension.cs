using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public static class FillDataExtension
    {
        public static void FillData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Login = "Ivan",
                    Password = "1111",
                    Id = 1,
                    FirstName = "Иван",
                    LastName = "Петров",
                    Phone = "+7(999) 198-56-42",
                    NumberOfBooks = 0,
                    MaxNumberOfBooks = 2,
                    Role = Role.Student
                });
            
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Login = "Alexey",
                    Password = "2222",
                    Id = 2,
                    FirstName = "Алексей",
                    LastName = "Сидоров",
                    Phone = "+7(987) 674-20-15",
                    NumberOfBooks = 0,
                    MaxNumberOfBooks = 2,
                    Role = Role.Student
                });

            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Login = "Mary",
                    Password = "0000",
                    Id = 3,
                    FirstName = "Мария",
                    LastName = "Савельева",
                    Phone = "+7(987) 111-11-11",
                    NumberOfBooks = 0,
                    MaxNumberOfBooks = 5,
                    Role = Role.Teacher
                });
            
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Login = "Hope",
                    Password = "0000",
                    Id = 4,
                    FirstName = "Надежда",
                    LastName = "Павлова",
                    Phone = "+7(987) 222-222-22",
                    NumberOfBooks = 0,
                    MaxNumberOfBooks = 0,
                    Role = Role.Librarian
                });
            
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Login = "Danis",
                    Password = "7777",
                    Id = 5,
                    FirstName = "Данис",
                    LastName = "Мингалеев",
                    Phone = "+7(987) 777-77-77",
                    NumberOfBooks = 0,
                    MaxNumberOfBooks = 0,
                    Role = Role.Admin
                });

            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = 1,
                    Name = "Война и мир",
                    Author = "Л.Н. Толстой",
                    YearOfPublishing = 1869,
                    NumberOfPages = 500,
                    NumberOfCopies = 5
                });

            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = 2,
                    Name = "Герой нашего времени",
                    Author = "М.Ю. Лермонтов",
                    YearOfPublishing = 1840,
                    NumberOfPages = 150,
                    NumberOfCopies = 2
                });
            
            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = 3,
                    Name = "Евгений Онегин",
                    Author = "А.С. Пушкин",
                    YearOfPublishing = 1831,
                    NumberOfPages = 90,
                    NumberOfCopies = 3
                });

            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = 4,
                    Name = "Идиот",
                    Author = "Ф.М. Достоевский",
                    YearOfPublishing = 1868,
                    NumberOfPages = 80,
                    NumberOfCopies = 1
                });
            
            for (int i = 1; i <= 5; i++)
            {
                modelBuilder.Entity<Copy>().HasData(
                    new Copy
                    {
                        Id = i,
                        BookId = 1,
                        AreAvailable = true
                    });
            }
        }
    }
}
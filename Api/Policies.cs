﻿namespace Api
{
    public class Policies
    {
        public const string RequireLibrarianPolicy = "RequireLibrarianRole";
        public const string RequireStudentOrTeacherPolicy = "RequireStudentOrTeacherRole";
        public const string RequireAdminPolicy = "RequireAdminRole";
    }
}
using System;
using System.IO;
using System.Text;
using Api.Domain.Books;
using Api.Domain.Books.Commands;
using Api.Domain.Copies;
using Api.Domain.Copies.Commands;
using Api.Domain.Issuances;
using Api.Domain.Issuances.Commands;
using Api.Domain.Users;
using Api.Domain.Users.Commands;
using Api.Extensions;
using Api.Requirements;
using Api.Services;
using DataAccessLayer;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appOptions = new AppOptions();
            Configuration.Bind(appOptions);
            
            services.AddDbContext<DataContext>(builder =>
                builder.UseNpgsql(appOptions.DbConnection));

            services.Configure<AppOptions>(Configuration);
            services.AddScoped<IJwtGenerator, JwtGenerator>();
            
            services.AddScoped<IAuthorizationHandler, LibrarianRoleRequirementHandler>();
            services.AddScoped<IAuthorizationHandler, StudentOrTeacherRoleRequirementHandler>();
            services.AddScoped<IAuthorizationHandler, AdminRoleRequirementHandler>();
            
            services.AddMediatR(
                typeof(CreateBookCommand), typeof(UpdateBookCommand), typeof(DeleteBookCommand),
                typeof(CreateIssuanceCommand), typeof(UpdateIssuanceCommand), typeof(DeleteIssuanceCommand),
                typeof(CreateUserCommand), typeof(UpdateUserCommand), typeof(DeleteUserCommand),
                typeof(CreateCopyCommand), typeof(UpdateCopyCommand), typeof(DeleteCopyCommand)
            );

            services.AddControllers()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateUserCommandValidator>())
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UpdateUserCommandValidator>())
                .AddFluentValidation(
                    fv => fv.RegisterValidatorsFromAssemblyContaining<CreateIssuanceCommandValidator>())
                .AddFluentValidation(
                    fv => fv.RegisterValidatorsFromAssemblyContaining<UpdateIssuanceCommandValidator>())
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateBookCommandValidator>())
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UpdateBookCommandValidator>())
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateCopyCommandValidator>())
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UpdateCopyCommandValidator>());

            services.AddAutoMapper(typeof(BookProfile), typeof(IssuanceProfile), typeof(UserProfile), typeof(CopyProfile));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Api", Version = "v1"});
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Api.xml"));
                
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In           = ParameterLocation.Header,
                    Description  = "Please insert token",
                    Name         = "Authorization",
                    Type         = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT"
                });
                
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                        },
                        new string[] { }
                    }
                });
            });
            
            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme  = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme     = JwtBearerDefaults.AuthenticationScheme;
                    x.RequireAuthenticatedSignIn = false;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken            = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.
                            GetBytes("my-secret-key-key-key-key-key-key-key-key")),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            
            services.AddAuthorization(options => 
                options.AddPolicy(Policies.RequireLibrarianPolicy,
                    builder => builder.AddRequirements(new LibrarianRoleRequirement("Librarian"))));
            services.AddAuthorization(options => 
                options.AddPolicy(Policies.RequireStudentOrTeacherPolicy,
                    builder => builder.AddRequirements(new StudentOrTeacherRoleRequirement("Student","Teacher"))));
            services.AddAuthorization(options => 
                options.AddPolicy(Policies.RequireAdminPolicy,
                    builder => builder.AddRequirements(new AdminRoleRequirement("Admin"))));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1"));
            }

            app.UseExceptionHandlers();
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
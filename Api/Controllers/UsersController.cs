﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Domain.Users;
using Api.Domain.Users.Commands;
using Api.Domain.Users.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Пользователи
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Регистрация нового ползователя (доступно только администратору)
        /// </summary>
        /// <param name="createUserCommand"></param>
        /// <response code="200">Пользователь успешно зарегистрирован</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <returns></returns>
        //[Authorize(Roles = "Admin")]
        [Authorize(Policy = Policies.RequireAdminPolicy)]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Create([FromQuery]CreateUserCommand createUserCommand)
        {
            await _mediator.Send(createUserCommand);
            return Ok("Пользователь успешно зарегистрирован!");
        }
        
        /// <summary>
        /// Получение списка всех пользователей хранящихся в базе
        /// </summary>
        /// <response code="200">Получен список всех пользователей</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="404">Пусто! В базе нет зарегистрированных пользователей</response>
        /// <returns></returns>
        [Authorize(Roles = "Student, Teacher, Librarian, Admin")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<UserDetailDto>),200)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll()
        {
            var query = new ListUsersQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }
        
        /// <summary>
        /// Обновление информации конкретного пользователя (доступно только администратору)
        /// </summary>
        /// <param name="updateUserCommand"></param>
        /// <response code="200">Данные пользователя успешно обновлены</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Пользователя с таким id не существует</response>
        /// <returns></returns>
        //[Authorize(Roles = "Admin")]
        [Authorize(Policy = Policies.RequireAdminPolicy)]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateUserCommand updateUserCommand)
        {
            await _mediator.Send(updateUserCommand);
            return Ok("Данные пользователя успешно обновлены!");
        }
        
        
        /// <summary>
        /// Удаление пользователя из базы (доступно только администратору)
        /// </summary>
        /// <param name="id">Идентификационный номер пользователя</param>
        /// <response code="200">Пользователь успешно удален</response>
        /// <response code="400">Нельзя удалить пользователя пока он не вернет все книги</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Пользователя с таким id не существует</response>
        /// <returns></returns>
        //[Authorize(Roles = "Admin")]
        [Authorize(Policy = Policies.RequireAdminPolicy)]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var deleteUserCommand = new DeleteUserCommand(id);
            await _mediator.Send(deleteUserCommand);
            return Ok("Пользователь успешно удален!");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Domain.Books;
using Api.Domain.Books.Commands;
using Api.Domain.Books.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Книги
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BooksController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Добавление новой книги в каталог
        /// </summary>
        /// <param name="createBookCommand"></param>
        /// <response code="200">Книга успешно добавлена в каталог</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Create([FromQuery]CreateBookCommand createBookCommand)
        {
            await _mediator.Send(createBookCommand);
            return Ok("Книга успешно добавлена в каталог!");
        }

        /// <summary>
        /// Получение списка всех возможных книг в каталоге по дате публикации 
        /// </summary>
        /// <response code="200">Получен список всех книг из каталога</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="404">Каталог книг пуст!</response>
        /// <returns></returns>
        [Authorize(Roles = "Student, Teacher, Librarian, Admin")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BookDetailsDto>), 200)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll()
        {
            var query = new ListBooksQueryDatePublishing();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Обновление информации о книге
        /// </summary>
        /// <param name="updateBookCommand"></param>
        /// <response code="200">Информация о книге успешно обновлена</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Книги с таким id не существует</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateBookCommand updateBookCommand)
        {
            await _mediator.Send(updateBookCommand);
            return Ok("Информация о книге успешно обновлена!");
        }
        
        /// <summary>
        /// Удаление книги из каталога
        /// </summary>
        /// <param name="id">Идентификационный номер книги</param>
        /// <response code="200">Книга успешно удалена из каталога</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Книги с таким id не существует</response>
        /// <returns></returns>
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var deleteBookCommand = new DeleteBookCommand(id);
            await _mediator.Send(deleteBookCommand);
            return Ok("Книга успешно удалена из каталога!");
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Domain.Copies;
using Api.Domain.Copies.Commands;
using Api.Domain.Copies.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Экземпляры книг
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CopiesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CopiesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Добавление экземпляра книги в библиотеку
        /// </summary>
        /// <param name="createCopyCommand"></param>
        /// <response code="200">Экземпляр книги успешно добавлен</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Create([FromQuery]CreateCopyCommand createCopyCommand)
        {
            await _mediator.Send(createCopyCommand);
            return Ok("Экземпляр книги успешно добавлен!");
        }

        /// <summary>
        /// Получение списка всех экземпляров книг, хранящихся в библиотеке
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Получен список всех экземпляров книг хранящихся в библиотеке</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="404">Пусто! В базе нет ни одного экземпляра книги</response>
        [Authorize(Roles = "Student, Teacher, Librarian, Admin")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CopyDetailsDto>), 200)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll()
        {
            var query = new ListCopyQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        
        /// <summary>
        /// Обвноление данных экземпляра книги
        /// </summary>
        /// <param name="updateCopyCommand"></param>
        /// <response code="200">Данные экземпляра книги успешно обновлены</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Экземпляра данной книги не существует</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateCopyCommand updateCopyCommand)
        {
            await _mediator.Send(updateCopyCommand);
            return Ok("Данные экземпляра книги успешно обновлены!");
        }
        
        
        /// <summary>
        /// Удаление экземпляра книги
        /// </summary>
        /// <param name="id">Инвентарный номер экземпляра</param>
        /// <response code="200">Экземпляр книги успешно удален из архива</response>
        /// <response code="400">Перед удалением экземпляра книги из библиотеки, необходимо её сначала вернуть</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Экземпляра с таким инвентарным номером не существует</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var deleteCopyCommand = new DeleteCopyCommand(id);
            await _mediator.Send(deleteCopyCommand);
            return Ok("Экземпляр книги успешно удален из библиотеки!");
        }
    }
}
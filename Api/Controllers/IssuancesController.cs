﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Domain.Issuances;
using Api.Domain.Issuances.Commands;
using Api.Domain.Issuances.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Выдача книг
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class IssuancesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public IssuancesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Выдача экземпляра книги пользователю
        /// </summary>
        /// <param name="createIssuanceCommand"></param>
        /// <response code="200">Книга успешно выдана пользователю</response>
        /// <response code="400">Нельзя выдать пользователю больше книг чем ему положено, либо этой книги нет в наличии</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code="404">Нельзя выдать книгу несуществующему пользователю, либо выдать несуществующую книгу</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Create([FromQuery]CreateIssuanceCommand createIssuanceCommand)
        {
            await _mediator.Send(createIssuanceCommand);
            return Ok("Книга успешно выдана пользователю!");
        }

        /// <summary>
        /// Получение списка всех выданных книг
        /// </summary>
        /// <response code="200">Получен список о выдаче всех книг</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="404">Пусто! В базе нет записей о выдаче книг</response>
        /// <returns></returns>
        [Authorize(Roles = "Student, Teacher, Librarian, Admin")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<IssuanceDetailsDto>),200)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll()
        {
            var query = new ListIssuanceQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Обновление информации записи выдачи книг
        /// </summary>
        /// <param name="updateIssuanceCommand"></param>
        /// <response code="200">Запись выдачи успешно обновлена</response>
        /// <response code="400">Неправильный, некорректный запрос</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Такой записи не существует</response>
        /// <returns></returns>
        //[Authorize(Roles = "Librarian")]
        [Authorize(Policy = Policies.RequireLibrarianPolicy)]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateIssuanceCommand updateIssuanceCommand)
        {
            await _mediator.Send(updateIssuanceCommand);
            return Ok("Запись выдачи успешно обновлена!");
        }

        /// <summary>
        /// Возврат экземпляра книги в библиотеку
        /// </summary>
        /// <param name="id">Инвентарный номер экземпляра</param>
        /// <response code="200">Книга успешно возвращена в библиотеку</response>
        /// <response code="401">Пользователь не авторизован</response>
        /// <response code="403">Данная функция недоступна для вашей роли</response>
        /// <response code ="404">Вы не забирали данный экземпляр из библиотеки</response>
        /// <returns></returns>
        //[Authorize(Roles = "Student, Teacher")]
        [Authorize(Policy = Policies.RequireStudentOrTeacherPolicy)]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var deleteIssuanceCommand = new DeleteIssuanceCommand(id);
            await _mediator.Send(deleteIssuanceCommand);
            return Ok("Книга успешно возвращена в библиотеку!");
        }
    }
}
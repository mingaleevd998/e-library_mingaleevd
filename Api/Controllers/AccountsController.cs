﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Api.Domain.Accounts.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Аутентификация пользователя
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <response code="200">Аутентификация прошла успешно!</response>
        /// <response code="400">Неправильно введены логин или пароль. Введите еще раз...</response>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("/login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<string>> Login(string login, string password)
        {
            try
            {
                var jwt =  await _mediator.Send(new LoginRequest(login,password));
                return Ok("Аутентификация прошла успешно!\n" + jwt);
            }
            catch (Exception e)
            {
                return BadRequest("Неправильно введены логин или пароль. Введите еще раз...");
            }
        }
    }
}
﻿using System;

namespace Api.Exceptions
{
    public class ForbiddenException : ApplicationException
    {
        public ForbiddenException(string message): base(message)
        {
            
        }
    }
}
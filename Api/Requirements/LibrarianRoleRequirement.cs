﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Api.Exceptions;
using Microsoft.AspNetCore.Authorization;

namespace Api.Requirements
{
    public class LibrarianRoleRequirement : IAuthorizationRequirement
    {
        public string Role { get; set; }

        public LibrarianRoleRequirement(string role)
        {
            Role = role;
        }
    }

    public class LibrarianRoleRequirementHandler : AuthorizationHandler<LibrarianRoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, LibrarianRoleRequirement requirement)
        {
            var claim = context.User.Claims.FirstOrDefault(claim1 => claim1.Type == ClaimsIdentity.DefaultRoleClaimType);
            if (claim != null)
            {
                if (claim.Value == requirement.Role)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    throw new ForbiddenException("Данная функция недоступна для вашей роли");
                }
            }

            return Task.CompletedTask;
        }
    }
}
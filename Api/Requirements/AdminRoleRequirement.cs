﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Api.Exceptions;
using Microsoft.AspNetCore.Authorization;

namespace Api.Requirements
{
    public class AdminRoleRequirement : IAuthorizationRequirement
    {
        public string AdminRole { get; set; }
        
        public AdminRoleRequirement(string adminRole)
        {
            AdminRole = adminRole;
        }
    }

    public class AdminRoleRequirementHandler : AuthorizationHandler<AdminRoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminRoleRequirement requirement)
        {
            var claim = context.User.Claims.FirstOrDefault(claim1 => claim1.Type == ClaimsIdentity.DefaultRoleClaimType);
            if (claim != null)
            {
                if (claim.Value == requirement.AdminRole)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    throw new ForbiddenException("Данная функция недоступна для вашей роли");
                }
            }

            return Task.CompletedTask;
        }
    }
}
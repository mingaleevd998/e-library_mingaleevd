﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Api.Exceptions;
using Microsoft.AspNetCore.Authorization;

namespace Api.Requirements
{
    public class StudentOrTeacherRoleRequirement: IAuthorizationRequirement
    {
        public string StudentRole { get; set; }
        public string TeacherRole { get; set; }
        
        public StudentOrTeacherRoleRequirement(string studentRole, string teacherRole)
        {
            StudentRole = studentRole;
            TeacherRole = teacherRole;
        }
    }

    public class StudentOrTeacherRoleRequirementHandler : AuthorizationHandler<StudentOrTeacherRoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, StudentOrTeacherRoleRequirement requirement)
        {
            var claim = context.User.Claims.FirstOrDefault(claim1 => claim1.Type == ClaimsIdentity.DefaultRoleClaimType);
            if (claim != null)
            {
                if (claim.Value == requirement.StudentRole || claim.Value == requirement.TeacherRole)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    throw new ForbiddenException("Данная функция недоступна для вашей роли");
                }
            }

            return Task.CompletedTask;
        }
    }
}
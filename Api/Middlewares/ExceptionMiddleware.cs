﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Api.Exceptions;
using Microsoft.AspNetCore.Http;

namespace Api.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (NotFoundException e)
            {
                context.Response.StatusCode = StatusCodes.Status404NotFound;
                var content = JsonSerializer.Serialize(new {Error = e.Message});
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(content);
            }
            catch (ForbiddenException e)
            {
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                var content = JsonSerializer.Serialize(new {Error = e.Message});
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(content);
            }
            catch (BadRequestException e)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                var content = JsonSerializer.Serialize(new {Error = e.Message});
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(content);
            }
            catch (Exception e)
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                var content = JsonSerializer.Serialize(new {Error = e.Message});
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(content);
            }
        }
    }
}
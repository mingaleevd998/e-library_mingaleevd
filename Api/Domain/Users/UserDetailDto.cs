﻿using DataAccessLayer.Models;
using FluentValidation;

namespace Api.Domain.Users
{
    public class UserDetailDto
    {
        public string Login { get; set; }
        
        public string Password { get; set; }
        
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public int NumberOfBooks { get; set; }
        
        public int MaxNumberOfBooks { get; set; }

        public string Role { get; set; }
    }
}
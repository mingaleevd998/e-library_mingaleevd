﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Users
{
    public class UserProfile: Profile
    {
        public UserProfile()
        {
            CreateMap<UserDetailDto, User>();
            CreateMap<User, UserDetailDto>();
        }
    }
}
﻿using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;

namespace Api.Domain.Users.Commands
{
    public class CreateUserCommand : IRequest
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Роль
        /// </summary>
        public int Role { get; set; }
    }

    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(dto => dto.Login).NotNull().NotEmpty().MinimumLength(2);
            RuleFor(dto => dto.Password).NotNull().NotEmpty().MinimumLength(4);
            RuleFor(dto => dto.FirstName).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
            RuleFor(dto => dto.LastName).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
            RuleFor(dto => dto.Phone).MinimumLength(5).MaximumLength(15);
            RuleFor(dto => dto.Role).LessThan(3).GreaterThan(-1);
        }
    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand>
    {
        private readonly DataContext _dataContext;
        private int _maxNumberOfBooks;

        public CreateUserCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            switch ((Role)request.Role)
            {
                case Role.Student: _maxNumberOfBooks = 2; break;
                case Role.Teacher: _maxNumberOfBooks = 5; break;
            }

            await _dataContext.Users.AddAsync(new User
            {
                Login = request.Login,
                Password = request.Password,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Phone = request.Phone,
                MaxNumberOfBooks = _maxNumberOfBooks,
                Role = (Role)request.Role
            }, cancellationToken);
            
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
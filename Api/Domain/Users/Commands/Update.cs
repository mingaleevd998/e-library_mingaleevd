﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Extensions;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Users.Commands
{
    public class UpdateUserCommand : IRequest
    {
        /// <summary>
        /// Идентификационный номер пользователя
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }
    }
    
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(dto => dto.FirstName).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
            RuleFor(dto => dto.LastName).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
            RuleFor(dto => dto.Phone).MinimumLength(5).MaximumLength(15);
        }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
    {
        private readonly DataContext _dataContext;

        public UpdateUserCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                .FirstOrDefaultAsync(user1 => user1.Id == request.Id, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException("Пользователя с таким id не существует");
            }
 
            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Phone = request.Phone;
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Extensions;
using DataAccessLayer;
using MediatR;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Users.Commands
{
    public class DeleteUserCommand : IRequest
    {
        /// <summary>
        /// Идентификационный номер пользователя
        /// </summary>
        public int Id { get; }

        public DeleteUserCommand(int id)
        {
            Id = id;
        }
    }

    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand>
    {
        private readonly DataContext _dataContext;

        public DeleteUserCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                .FirstOrDefaultAsync(user1 => user1.Id == request.Id, cancellationToken);
            if (user == null) throw new NotFoundException("Пользователя с таким id не существует");

            if (user.NumberOfBooks > 0) 
                throw new BadRequestException("Нельзя удалить пользователя пока он не вернет все книги");
            
            _dataContext.Users.Remove(user);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Users.Queries
{
    public class ListUsersQuery : IRequest<IEnumerable<UserDetailDto>>
    {
        
    }

    public class ListUsersQueryHandler : IRequestHandler<ListUsersQuery, IEnumerable<UserDetailDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListUsersQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDetailDto>> Handle(ListUsersQuery request, CancellationToken cancellationToken)
        {
            var usersData = await _dataContext.Users
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            if (!usersData.Any()) throw new NotFoundException("Пусто! В базе нет зарегистрированных пользователей");
            
            return _mapper.Map<IEnumerable<UserDetailDto>>(usersData);
        }
    }
}
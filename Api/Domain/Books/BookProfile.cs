﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Books
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<BookDetailsDto, Book>();
            CreateMap<Book, BookDetailsDto>();
        }
    }
}
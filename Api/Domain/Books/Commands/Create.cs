﻿using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;

namespace Api.Domain.Books.Commands
{
    public class CreateBookCommand : IRequest
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public int YearOfPublishing { get; set; }
        public int NumberOfPages { get; set; }
        public int NumberOfCopies { get; set; }
    }
    
    public class CreateBookCommandValidator : AbstractValidator<BookDetailsDto>
    {
        public CreateBookCommandValidator()
        {
            RuleFor(dto => dto.Name).NotNull().NotEmpty().MinimumLength(1).MaximumLength(50);
            RuleFor(dto => dto.Author).NotNull().NotEmpty().MinimumLength(1).MaximumLength(50);
            RuleFor(dto => dto.YearOfPublishing).NotNull().NotEmpty().LessThanOrEqualTo(2022).GreaterThan(1500);
            RuleFor(dto => dto.NumberOfPages).GreaterThan(0);
            RuleFor(dto => dto.NumberOfCopies).NotNull().NotEmpty().GreaterThan(-1);
        }
    }
    
    public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand>
    {
        private readonly DataContext _dataContext;

        public CreateBookCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.Books.AddAsync(new Book
            {
                Name = request.Name,
                Author = request.Author,
                YearOfPublishing = request.YearOfPublishing,
                NumberOfPages = request.NumberOfPages,
                NumberOfCopies = request.NumberOfCopies
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
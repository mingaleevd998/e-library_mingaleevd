﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Extensions;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Books.Commands
{
    public class UpdateBookCommand : IRequest
    {
        /// <summary>
        /// Номер книги
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Название книги
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Автор
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Год издания
        /// </summary>
        public int YearOfPublishing { get; set; }
        /// <summary>
        /// Количество страниц
        /// </summary>
        public int NumberOfPages { get; set; }
        /// <summary>
        /// Количество экземпляров
        /// </summary>
        public int NumberOfCopies { get; set; }
    }
    
    public class UpdateBookCommandValidator : AbstractValidator<BookDetailsDto>
    {
        public UpdateBookCommandValidator()
        {
            RuleFor(dto => dto.Name).NotNull().NotEmpty().MinimumLength(1).MaximumLength(50);
            RuleFor(dto => dto.Author).NotNull().NotEmpty().MinimumLength(1).MaximumLength(50);
            RuleFor(dto => dto.YearOfPublishing).NotNull().NotEmpty().LessThanOrEqualTo(2022).GreaterThan(1500);
            RuleFor(dto => dto.NumberOfPages).GreaterThan(0);
            RuleFor(dto => dto.NumberOfCopies).NotNull().NotEmpty().GreaterThan(-1);
        }
    }

    public class UpdateBookCommandHandler : IRequestHandler<UpdateBookCommand>
    {
        private readonly DataContext _dataContext;

        public UpdateBookCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
        {
            var book = await _dataContext.Books
                .FirstOrDefaultAsync(book1 => book1.Id == request.Id, cancellationToken);

            if (book == null)
            {
                throw new NotFoundException("Книги с таким номером не существует");
            }

            book.Name = request.Name;
            book.Author = request.Author;
            book.YearOfPublishing = request.YearOfPublishing;
            book.NumberOfPages = request.NumberOfPages;
            book.NumberOfCopies = request.NumberOfCopies;
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Extensions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Books.Commands
{
    public class DeleteBookCommand : IRequest
    {
        /// <summary>
        /// Номер книги
        /// </summary>
        public int Id { get; }

        public DeleteBookCommand(int id)
        {
            Id = id;
        }
    }

    public class DeleteBookCommandHandler : IRequestHandler<DeleteBookCommand>
    {
        private readonly DataContext _dataContext;

        public DeleteBookCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(DeleteBookCommand request, CancellationToken cancellationToken)
        {
            var book = await _dataContext.Books
                .FirstOrDefaultAsync(book1 => book1.Id == request.Id, cancellationToken);

            if (book == null)
            {
                throw new NotFoundException("Книги с таким номером не существует");
            }

            _dataContext.Books.Remove(book);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
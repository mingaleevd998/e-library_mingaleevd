﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Books.Queries
{
    public class ListBooksQueryDatePublishing : IRequest<IEnumerable<BookDetailsDto>>
    {
        
    }

    public class ListBooksQueryHandler : IRequestHandler<ListBooksQueryDatePublishing, IEnumerable<BookDetailsDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListBooksQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<BookDetailsDto>> Handle(ListBooksQueryDatePublishing request, CancellationToken cancellationToken)
        {
            var booksData = await _dataContext.Books
                .OrderBy(book => book.YearOfPublishing)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            if (!booksData.Any()) throw new NotFoundException("Каталог книг пуст!");

            return _mapper.Map<IEnumerable<BookDetailsDto>>(booksData);
        }
    }
}
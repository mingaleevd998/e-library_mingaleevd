﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Issuances.Commands
{
    public class CreateIssuanceCommand : IRequest
    {
        /// <summary>
        /// Идентификационный номер пользователя
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Инвентарный номер экземпляра книги
        /// </summary>
        public int CopyId { get; set; }
        /// <summary>
        /// Дата выдачи экземпляра книги
        /// </summary>
        public string DateOfIssue { get; set; }
    }

    public class CreateIssuanceCommandValidator : AbstractValidator<IssuanceDetailsDto>
    {
        public CreateIssuanceCommandValidator()
        {
            RuleFor(dto => dto.UserId).NotNull().NotEmpty();
            RuleFor(dto => dto.CopyId).NotNull().NotEmpty();
            RuleFor(dto => dto.DateOfIssue).MinimumLength(8).MaximumLength(30);
        }
    }
    
    public class CreateIssuanceCommandHandler : IRequestHandler<CreateIssuanceCommand>
    {
        private readonly DataContext _dataContext;

        public CreateIssuanceCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateIssuanceCommand request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                .FirstOrDefaultAsync(user => user.Id == request.UserId, cancellationToken: cancellationToken);
            if (user == null) throw new NotFoundException("Нельзя выдать книгу несуществующему пользователю");

            var copy = await _dataContext.Copies
                .FirstOrDefaultAsync(copy1 =>  copy1.Id == request.CopyId, cancellationToken: cancellationToken);
            if (copy == null) throw new NotFoundException("Нельзя выдать несуществующий экземпляр книги");
            
            // если экземпляр книги есть в наличии
            if (copy.AreAvailable == true)
            {
                // если пользователь забрал максимальное количество книг которое ему положено
                if (user.NumberOfBooks >= user.MaxNumberOfBooks)
                {
                    throw new BadRequestException($"Нельзя! Пусть вернет для начала старые книги. У него их аж {user.NumberOfBooks} штук/и");
                }
                
                user.NumberOfBooks++; // увеличение количества забранных книг у пользовател
                copy.AreAvailable = false; // теперь экземпляр книги недоступен в библиотеке, так как его забрал пользователь

                await _dataContext.Issuance.AddAsync(new Issuance()
                {
                    UserId = request.UserId,
                    CopyId = request.CopyId,
                    DateOfIssue = request.DateOfIssue,
                }, cancellationToken);
                await _dataContext.SaveChangesAsync(cancellationToken);
            }
            else
            {
                throw new BadRequestException("Эту книгу уже взял кто-то другой");
            }

            return Unit.Value;
        }
    }
}
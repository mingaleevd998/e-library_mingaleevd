﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Issuances.Commands
{
    public class UpdateIssuanceCommand: IRequest
    {
        /// <summary>
        /// Номер записи о выдаче
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Дата выдачи экземпляра книги
        /// </summary>
        public string DateOfIssue { get; set; }
    }
    
    public class  UpdateIssuanceCommandValidator : AbstractValidator<IssuanceDetailsDto>
    {
        public  UpdateIssuanceCommandValidator()
        {
            RuleFor(dto => dto.DateOfIssue).MinimumLength(8).MaximumLength(30);
        }
    }

    public class UpdateIssuanceCommandHandler : IRequestHandler<UpdateIssuanceCommand>
    {
        private readonly DataContext _dataContext;

        public UpdateIssuanceCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(UpdateIssuanceCommand request, CancellationToken cancellationToken)
        {
            var issuance = await _dataContext.Issuance
                .FirstOrDefaultAsync(issuance1 => issuance1.Id == request.Id, cancellationToken);

            if (issuance == null)
            {
                throw new NotFoundException("Такой записи не существует!");
            }
            
            issuance.DateOfIssue = request.DateOfIssue;
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
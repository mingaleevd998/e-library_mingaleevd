﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Issuances.Commands
{
    public class DeleteIssuanceCommand : IRequest
    {
        /// <summary>
        /// Инвентарный номер экземпляра
        /// </summary>
        public int Id { get; }

        public DeleteIssuanceCommand(int id)
        {
            Id = id;
        }
    }

    public class DeleteIssuanceCommandHandler : IRequestHandler<DeleteIssuanceCommand>
    {
        private readonly DataContext _dataContext;

        public DeleteIssuanceCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(DeleteIssuanceCommand request, CancellationToken cancellationToken)
        {
            var issuance = await _dataContext.Issuance
                .FirstOrDefaultAsync(issuance1 => issuance1.CopyId == request.Id, cancellationToken);
            if (issuance == null) throw new NotFoundException("Вы не забирали данный экземпляр из библиотеки");
            
            // находим пользователя, который прикреплен к нашей записи и уменьшаем его счетчик задолженностей
            var user = await _dataContext.Users
                .FirstOrDefaultAsync(user1 => user1.Id == issuance.UserId, cancellationToken: cancellationToken);
            user.NumberOfBooks--;

            // находим копию книги в записи и возвращаем в состояние "есть в наличии"
            var copy = await _dataContext.Copies
                .FirstOrDefaultAsync(copy1 => copy1.Id == issuance.CopyId, cancellationToken: cancellationToken);
            copy.AreAvailable = true;

            // удаление записи о выдаче книги
            _dataContext.Issuance.Remove(issuance);
            await _dataContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Issuances
{
    public class IssuanceProfile: Profile
    {
        public IssuanceProfile()
        {
            CreateMap<IssuanceDetailsDto, Issuance>();
            CreateMap<Issuance, IssuanceDetailsDto>();
        }
    }
}
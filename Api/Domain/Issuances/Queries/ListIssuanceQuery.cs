﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Issuances.Queries
{
    public class ListIssuanceQuery : IRequest<IEnumerable<IssuanceDetailsDto>>
    {
        
    }

    public class ListIssuanceQueryHandler : IRequestHandler<ListIssuanceQuery, IEnumerable<IssuanceDetailsDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListIssuanceQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<IssuanceDetailsDto>> Handle(ListIssuanceQuery request, CancellationToken cancellationToken)
        {
            var issuanceData = await _dataContext.Issuance
                .Include(issuance => issuance.Copy)
                .Include(issuance => issuance.User)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            if (!issuanceData.Any()) throw new NotFoundException("Пусто! В базе нет записей о выдаче книг");
            
            return _mapper.Map<IEnumerable<IssuanceDetailsDto>>(issuanceData);
        }
    }
}
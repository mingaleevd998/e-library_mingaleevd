﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Copies.Queries
{
    public class ListCopyQuery : IRequest<IEnumerable<CopyDetailsDto>>
    {
        
    }

    public class ListCopyQueryHandler : IRequestHandler<ListCopyQuery, IEnumerable<CopyDetailsDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListCopyQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CopyDetailsDto>> Handle(ListCopyQuery request, CancellationToken cancellationToken)
        {
            var copiesData = await _dataContext.Copies
                .Include(copy => copy.Book)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            if (!copiesData.Any()) throw new NotFoundException("Пусто! В базе нет ни одного экземпляра книги");
            
            return _mapper.Map<IEnumerable<CopyDetailsDto>>(copiesData);
        }
    }
}
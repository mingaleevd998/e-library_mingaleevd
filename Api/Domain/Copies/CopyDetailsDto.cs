﻿using DataAccessLayer.Models;

namespace Api.Domain.Copies
{
    public class CopyDetailsDto
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public bool AreAvailable { get; set; }
    }
}
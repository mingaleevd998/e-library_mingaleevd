﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Copies.Commands
{
    public class CreateCopyCommand : IRequest
    {
        /// <summary>
        /// Номер книги
        /// </summary>
        public int BookId { get; set; }
        /// <summary>
        /// Есть в наличии
        /// </summary>
        public bool AreAvailable { get; set; }
    }

    public class CreateCopyCommandValidator : AbstractValidator<CopyDetailsDto>
    {
        public CreateCopyCommandValidator()
        {
            RuleFor(dto => dto.BookId).NotNull().NotEmpty();
            RuleFor(dto => dto.AreAvailable).NotNull().NotEmpty();
        }
    }

    public class CreateCopyCommandHandler : IRequestHandler<CreateCopyCommand>
    {
        private readonly DataContext _dataContext;

        public CreateCopyCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateCopyCommand request, CancellationToken cancellationToken)
        {
            var book = await _dataContext.Books
                .FirstOrDefaultAsync(book1 => book1.Id == request.BookId, cancellationToken: cancellationToken);
            if (book == null) throw new NotFoundException("Ошибка! Такой книги не существует");

            var result = _dataContext.Copies
                .Select(i => i.Book.Id == book.Id).Count();
            if (result > book.NumberOfCopies)
            {
                throw new BadRequestException("Ошибка! Невозможно добавить больше экземпляров данной книги");
            }
            
            await _dataContext.Copies.AddAsync(new DataAccessLayer.Models.Copy()
            {
                BookId = request.BookId,
                AreAvailable = request.AreAvailable
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
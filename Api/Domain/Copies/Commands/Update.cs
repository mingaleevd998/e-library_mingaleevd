﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Copies.Commands
{
    public class UpdateCopyCommand : IRequest
    {
        /// <summary>
        /// Инвентарный номер экземпляра книги
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Номер книги
        /// </summary>
        public int BookId { get; set; }
        /// <summary>
        /// Есть в наличии
        /// </summary>
        public bool AreAvailable { get; set; }
    }

    public class UpdateCopyCommandValidator : AbstractValidator<CopyDetailsDto>
    {
        public UpdateCopyCommandValidator()
        {
            RuleFor(dto => dto.BookId).NotNull().NotEmpty();
            RuleFor(dto => dto.AreAvailable).NotNull().NotEmpty();
        }
    }

    public class UpdateCopyCommandHandler : IRequestHandler<UpdateCopyCommand>
    {
        private readonly DataContext _dataContext;

        public UpdateCopyCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(UpdateCopyCommand request, CancellationToken cancellationToken)
        {
            var copy = await _dataContext.Copies
                .FirstOrDefaultAsync(copy1 => copy1.Id == request.Id, cancellationToken);

            if (copy == null)
            {
                throw new NotFoundException("Экземпляра данной книги не существует");
            }

            copy.BookId = request.BookId;
            copy.AreAvailable = request.AreAvailable;
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
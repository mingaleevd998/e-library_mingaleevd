﻿using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Api.Domain.Copies.Commands
{
    public class DeleteCopyCommand : IRequest
    {
        /// <summary>
        /// Инвентарный номер экземпляра книги
        /// </summary>
        public int Id { get; }
        public DeleteCopyCommand(int id)
        {
            Id = id;
        }
    }

    public class DeleteCopyCommandHandler : IRequestHandler<DeleteCopyCommand>
    {
        private readonly DataContext _dataContext;

        public DeleteCopyCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public async Task<Unit> Handle(DeleteCopyCommand request, CancellationToken cancellationToken)
        {
            var copy = await _dataContext.Copies
                .FirstOrDefaultAsync(copy => copy.Id == request.Id, cancellationToken: cancellationToken);
            if (copy == null) throw new NotFoundException("Экземпляра с таким инвентарным номером не существует");

            if (copy.AreAvailable == false) 
                throw new BadRequestException("Перед удалением экземпляра книги из библиотеки, необходимо её сначала вернуть");
            
            _dataContext.Copies.Remove(copy);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
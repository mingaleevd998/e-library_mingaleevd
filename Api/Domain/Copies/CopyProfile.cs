﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Copies
{
    public class CopyProfile : Profile
    {
        public CopyProfile()
        {
            CreateMap<CopyDetailsDto, Copy>();
            CreateMap<Copy, CopyDetailsDto>();
        }
    }
}